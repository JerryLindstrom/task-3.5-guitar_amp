import guitar.*;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        //Making my GuitarCollection as an Arraylist so i can add more and more.. you never sell a bought guitar..
        //you keep adding to the collection.. :>
        ArrayList<Guitar> myGuitarCollection = new ArrayList<>();

        ElectricGuitar gibsonLP = new ElectricGuitar("Gibson", "Les Paul", -59, "PAF");
        ElectricGuitar gretschWF = new ElectricGuitar("Gretsch", "G6136 White Falcon", -86, "Filtertron");
        ElectricGuitar fenderStrat = new ElectricGuitar("Fender", "Stratocaster", -64, "60/63 single coil");
        ElectroAcousticGuitar gibsonJ45 = new ElectroAcousticGuitar("Gibson", "J-45", -19, "LR Baggs");
        AcousticGuitar martinD45 = new AcousticGuitar("Martin", "D-45", -67);
        AcousticGuitar gibsonJ200 = new AcousticGuitar("Gibson", "J-200", -94);

        myGuitarCollection.add(gibsonLP);
        myGuitarCollection.add(gretschWF);
        myGuitarCollection.add(fenderStrat);
        myGuitarCollection.add(martinD45);
        myGuitarCollection.add(gibsonJ45);
        myGuitarCollection.add(gibsonJ200);

        //If you would like to hear what sound each type of guitar makes.. remove the /* and */ "original task
        // but ii /* */ since annoying when trying the extra task..
        /*for (Guitar guitar:myGuitarCollection) {
            guitar.makeSound();
        }*/

        //Making an Ampcollection as an Arraylist.
        ArrayList<Amplifier> myAmpCollection = new ArrayList<>();

        Amplifier myAmpFender = new Amplifier("Fender", "57 Custom Deluxe", 7, 12);
        Amplifier myAmpMesa = new Amplifier("Mesa Boogie", "Badlander", 4, 50);
        Amplifier myAmpMarshall = new Amplifier("Marshall", "1962 Bluesbreaker", 11, 30);
        Amplifier myAmpVox = new Amplifier("Vox", "AC30", 6, 30);

        myAmpCollection.add(myAmpFender);
        myAmpCollection.add(myAmpMesa);
        myAmpCollection.add(myAmpMarshall);
        myAmpCollection.add(myAmpVox);

        /*for (Amplifier amplifier:myAmpCollection) {
            amplifier.makeSound();
        }*/
        System.out.println(gibsonLP);
        System.out.println(myAmpMesa);
        System.out.println(myAmpMarshall);
        System.out.println(myAmpMesa);
        System.out.println();

        // Setting the power on my Mesa to on but volume to 0 so it should not produce sound..
        myAmpMesa.setAmplifierPower(true);
        myAmpMesa.setAmplifierCurrentVolume(0);
        myAmpMesa.makeSound();
        System.out.println();

        // Keeping the power on but turns on volume 7 so it should make a static sound
        myAmpMesa.setAmplifierCurrentVolume(7);
        myAmpMesa.makeSound();

        //Trying to get this guitar plugged in to the amp and make "electric guitar sound" but failing miserable.."
        //So this is my progress so far..
        gibsonLP.setPluggedInAmp(true);
        myAmpMesa.makeSound();


    }

}
