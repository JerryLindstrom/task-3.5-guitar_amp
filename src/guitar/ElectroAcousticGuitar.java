package guitar;

public class ElectroAcousticGuitar extends Guitar {
    private String pickups;
    protected boolean pluggedInAmp;

    public ElectroAcousticGuitar(String guitarBrand, String guitarModel, int guitarYear, String pickups) {
        super(guitarBrand, guitarModel, guitarYear);
        this.pickups = pickups;
        pluggedInAmp = false;
    }

    @Override
    public void makeSound() {
        System.out.println("Fancier acoustic guitar sounds..");
    }

    //Just something i added so you could request info about your Guitar.
    @Override
    public String toString() {
        return guitarBrand + " " + guitarModel + " " + guitarYear + " with " + pickups + " pickups.";
    }

    public void setPluggedInAmp(boolean pluggedInAmp) {
        this.pluggedInAmp = pluggedInAmp;
    }

}
