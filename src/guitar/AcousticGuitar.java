package guitar;

public class AcousticGuitar extends Guitar {
    public AcousticGuitar(String guitarBrand, String guitarModel, int guitarYear) {
        super(guitarBrand, guitarModel, guitarYear);
    }

    @Override
    public void makeSound() {
        System.out.println("Acoustic Guitar sounds..");
    }

}
