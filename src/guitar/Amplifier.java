package guitar;

public class Amplifier implements Playable {
    private String amplifierBrand;
    private String amplifierModel;
    private int amplifierCurrentVolume;
    private int amplifierWattage;
    private boolean amplifierPower;
    private Guitar isPluggedInAmp; //is not doing what i want..

    public Amplifier(String amplifierBrand, String amplifierModel, int amplifierCurrentVolume,
                     int amplifierWattage) {
        this.amplifierBrand = amplifierBrand;
        this.amplifierModel = amplifierModel;
        this.amplifierCurrentVolume = amplifierCurrentVolume;
        this.amplifierWattage = amplifierWattage;
        amplifierPower = false;
        isPluggedInAmp = null;
    }

    public void setAmplifierPower(boolean amplifierPower) {
        this.amplifierPower = amplifierPower;
    }

    public void setAmplifierCurrentVolume(int amplifierCurrentVolume) {
        this.amplifierCurrentVolume = amplifierCurrentVolume;
    }

    //Just something i added so you could request info about your Amplifier.
    @Override
    public String toString() {
        return amplifierBrand + " " + amplifierModel + " " + amplifierWattage + " W, Volume " +
                amplifierCurrentVolume + " of 10";

    }

    @Override
    public void makeSound() {
        if (!amplifierPower  || amplifierCurrentVolume == 0) {
            return;
        }
        if (isPluggedInAmp == null) {
            System.out.println("STATIC NOICES..");
            System.out.println();
        }
        //this does not do anything yet.. since my knowledge is too limited yet.. :<
        //But here is where i would like to check if a guitar is plugged in and make some sweet hendrix noises..
       /* if (isPluggedInAmp != null) {
            makeSound();
        } */
    }


    // Please ignore this as this is my progress so far with the extra task....
    public void connectGuitar() {
        /*(some code to You should be able to change guitars dynamically with a method called connectGuitar(),
         but only ElectricGuitar and ElectroAcoustic guitars are able to be plugged in,
          otherwise throw a custom Exception called InvalidGuitarException)*/
        //  throw new InvalidGuitarException("You can only plug in electric guitars into amps");
    }

    public void disconnectGuitar() {
        isPluggedInAmp = null;
    }

}

