package guitar;

public abstract class Guitar implements Playable {
    protected String guitarBrand;
    protected String guitarModel;
    protected int guitarYear;

    public Guitar(String guitarBrand, String guitarModel, int guitarYear) {
        this.guitarBrand = guitarBrand;
        this.guitarModel = guitarModel;
        this.guitarYear = guitarYear;
    }

    //Just something i added so you could request info about your Guitar.
    @Override
    public String toString() {
        return guitarBrand + " " + guitarModel + " " + guitarYear;

    }


}
