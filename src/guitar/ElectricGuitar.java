package guitar;

public class ElectricGuitar extends Guitar{
    private String pickups;
    protected boolean pluggedInAmp;

    public ElectricGuitar(String guitarBrand, String guitarModel, int guitarYear, String pickups) {
        super(guitarBrand, guitarModel, guitarYear);
        this.pickups = pickups;
        pluggedInAmp = false;
    }

    @Override
    public void makeSound() {
        System.out.println("Electric Guitar sounds..");
    }

    //Just something i added so you could request info about your Guitar.
    @Override
    public String toString() {
        return guitarBrand + " " + guitarModel + " " + guitarYear + " with " + pickups + " pickups.";
    }

}
